# Laravel Active Collab

[![Build Status](https://travis-ci.org/cherrypulp/laravel-active-collab.svg?branch=master)](https://travis-ci.org/cherrypulp/laravel-active-collab)
[![styleci](https://styleci.io/repos/CHANGEME/shield)](https://styleci.io/repos/CHANGEME)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/cherrypulp/laravel-active-collab/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/cherrypulp/laravel-active-collab/?branch=master)
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/CHANGEME/mini.png)](https://insight.sensiolabs.com/projects/CHANGEME)
[![Coverage Status](https://coveralls.io/repos/github/cherrypulp/laravel-active-collab/badge.svg?branch=master)](https://coveralls.io/github/cherrypulp/laravel-active-collab?branch=master)

[![Packagist](https://img.shields.io/packagist/v/cherrypulp/laravel-active-collab.svg)](https://packagist.org/packages/cherrypulp/laravel-active-collab)
[![Packagist](https://poser.pugx.org/cherrypulp/laravel-active-collab/d/total.svg)](https://packagist.org/packages/cherrypulp/laravel-active-collab)
[![Packagist](https://img.shields.io/packagist/l/cherrypulp/laravel-active-collab.svg)](https://packagist.org/packages/cherrypulp/laravel-active-collab)

Package description: Wrapper for the Active Collab SDK, adapted to Laravel

## Installation

Install via composer
```bash
composer require cherrypulp/laravel-active-collab
```

### Register Service Provider

**Note! This and next step are optional if you use laravel>=5.5 with package
auto discovery feature.**

Add service provider to `config/app.php` in `providers` section
```php
Cherrypulp\LaravelActiveCollab\ServiceProvider::class,
```

### Register Facade

Register package facade in `config/app.php` in `aliases` section
```php
Cherrypulp\LaravelActiveCollab\Facades\LaravelActiveCollab::class,
```

### Publish Configuration File

```bash
php artisan vendor:publish --provider="Cherrypulp\LaravelActiveCollab\ServiceProvider" --tag="config"
```

## Usage

Update the config in your env file.

```
ACTIVECOLLAB_COMPANY=******
ACTIVECOLLAB_APP_NAME=******
ACTIVECOLLAB_MAIL=******
ACTIVECOLLAB_PASSWORD=******
ACTIVECOLLAB_URL=******
```

Available methods:
- getOpenProjects(): get all open projects
- getProjectTasks($project_id): get the open tasks from a project
- getCompanyUsers($company_id): get non-archived users of a company
- getUserByEmail($email): get specified user
- postTimeRecord($date, $duration, $user_id, $project_id, $task_id)
- deleteRecordsByDay($date, $user_id)

## Security

If you discover any security related issues, please email
instead of using the issue tracker.

## Credits

- Simon Vreux

This package is bootstrapped with the help of
[cherrypulp/laravel-package-generator](https://github.com/cherrypulp/laravel-package-generator).
