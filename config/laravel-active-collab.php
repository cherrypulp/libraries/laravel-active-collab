<?php

return [

    'company' => env('ACTIVECOLLAB_COMPANY'),
    'app_name' => env('ACTIVECOLLAB_APP_NAME'),
    'auth_mail' => env('ACTIVECOLLAB_MAIL'),
    'auth_pw' => env('ACTIVECOLLAB_PASSWORD'),
    'url' => env('ACTIVECOLLAB_URL'),

];
