<?php

namespace Cherrypulp\LaravelActiveCollab;

use ActiveCollab\SDK\Authenticator\SelfHosted;
use \ActiveCollab\SDK\Client;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    const CONFIG_PATH = __DIR__ . '/../config/laravel-active-collab.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_PATH => config_path('laravel-active-collab.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_PATH,
            'laravel-active-collab'
        );

        $this->app->bind('laravel-active-collab', function () {
            $client = new LaravelActiveCollab();
            return $client;
        });
    }
}
