<?php

namespace Cherrypulp\LaravelActiveCollab;

use ActiveCollab\SDK\Authenticator\SelfHosted;
use ActiveCollab\SDK\Client;
use Carbon\Carbon;

class LaravelActiveCollab extends Client {

    function __construct()
    {
        $authenticator = new SelfHosted(
            config('laravel-active-collab.company'),
            config('laravel-active-collab.app_name'),
            config('laravel-active-collab.auth_mail'),
            config('laravel-active-collab.auth_pw'),
            config('laravel-active-collab.url'),
        );

        $token = $authenticator->issueToken();

        parent::__construct($token);
    }

    /**
     * Get all open projects
     *
     * @return Array
     */
    public function getOpenProjects()
    {
        $projects = $this->get('projects')->getJson();
        usort($projects, function ($a, $b) {
            return strnatcmp($a['name'], $b['name']);
        });
        return $projects;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getProjectTasks($projectId)
    {
        return $this->get("projects/$projectId/tasks")->getJson()['tasks'];
    }

    /**
     * Undocumented function
     *
     * @param [type] $projectId
     * @param [type] $taskId
     * @return void
     */
    public function getTask($projectId, $taskId)
    {
        return $this->get("projects/$projectId/tasks/$taskId")->getJson()['single'];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getAllUsers()
    {
        return $this->get('users/all')->getJson();
    }

    /**
     * Undocumented function
     *
     * @param integer $companyID
     * @return void
     */
    public function getCompanyUsers($companyID = 1)
    {
        $users = $this->getAllUsers();
        foreach($users as $key => $value) {
            if ($value['company_id'] != $companyID || $value['is_archived']) {
                unset($users[$key]);
            }
        }

        return $users;
    }

    /**
     * Undocumented function
     *
     * @param [type] $email
     * @return void
     */
    public function getUserByEmail($email)
    {
        $users = $this->getAllUsers();

        foreach ($users as $key => $value) {
            if ($value['email'] === $email) {
                return $value;
            }
        }

        return null;
    }

    /**
     * Undocumented function
     *
     * @param [type] $date
     * @param [type] $duration
     * @param [type] $projectId
     * @param [type] $taskId
     * @return void
     */
    public function postTimeRecord($date, $duration, $userId, $projectId, $taskId)
    {
        try {
            $record = $this->post("projects/$projectId/time-records", [
              'value'       => $duration,
              'record_date' => $date,
              'task_id'     => $taskId,
              'job_type_id' => 1,
              'user_id'     => $userId,
            ]);
            return $record;
        } catch(AppException $e) {
            print $e->getMessage();
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $date
     * @param [type] $userId
     * @return void
     */
    public function deleteRecordsByDay($date, $userId)
    {
        $timestamp = Carbon::parse($date)->timestamp;
        $records = $this->get('users/' . $userId . '/time-records' )->getJson()['time_records'];
        $records = array_filter($records, function($a) use ($timestamp) {
            return $a['record_date'] == $timestamp;
        });
        foreach($records as $value) {
            $this->delete($value['url_path']);
        }
    }

}
