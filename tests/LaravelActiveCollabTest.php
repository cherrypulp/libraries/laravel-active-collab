<?php

namespace Cherrypulp\LaravelActiveCollab\Tests;

use Cherrypulp\LaravelActiveCollab\Facades\LaravelActiveCollab;
use Cherrypulp\LaravelActiveCollab\ServiceProvider;
use Orchestra\Testbench\TestCase;

class LaravelActiveCollabTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'laravel-active-collab' => LaravelActiveCollab::class,
        ];
    }

    public function testExample()
    {
        $this->assertEquals(1, 1);
    }
}
